## Sobre o projeto

Este é um simples Helloworld project baseado no GitLab Spring template.

## Objetivo

O objetivo do projeto é realizar o Continuous Integration(CI) e o Continuous Deployment(CD) de forma automatizada através do Auto DevOps baseados nas alterações do projeto durante o tempo.

## Stacks

Neste projeto as seguinte stacks(tecnologias) foram utilizadas:
 - Java Spring
 - Docker
 - Kubernetes(K8S)
 - Rest API
 - GitLab
 - Google Kubernetes Engine(GKE)

## Pipeline Workflow

O projeto atual possui 4 estágios de pipeline baseados no arquivo *.gitlab-ci.yml*:
 - *Build:* a aplicação realiza o build de uma imagem Docker e executa o upload no projeto
 - *Test:* jobs(tarefas) capazes de disparar testes de integração e de unidade no código da aplicação
 - *Deploy:* após a execução dos testes finalizarem, a aplicação é implantada no K8S através do cluster do Google Kubernetes Engine
 - *Performance:* nesse estágio são executados testes de performance na aplicação após ser implantada(deployed)

*Nota*: Cada vez que o projeto sofre alguma mudança em sua estrutura monitorada pelo git, então um novo pipeline workflow é executado garantindo assim a integridade, segurança conformidade e disponibilidade da aplicação.
